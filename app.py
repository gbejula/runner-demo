from flask import Flask
import os

app = Flask(__name__)

@app.route("/")
def wish():
        message = "{name} is a Gitlab pro!"
        return message.format(name=os.getenv("NAME", "Tayo"))

if __name__ == "__main__":
        app.run(host='0.0.0.0', port=8080)
